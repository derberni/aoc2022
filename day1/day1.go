package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
)

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer func(fp *os.File) {
		_ = fp.Close()
	}(fp)

	var elves []int

	currentCal := 0
	var cal int
	for {
		_, err := fmt.Fscanln(fp, &cal)
		if err != nil {
			elves = append(elves, currentCal)
			if errors.Is(err, io.EOF) {
				break
			}
			currentCal = 0
			continue
		}
		currentCal += cal
	}

	sort.Sort(sort.IntSlice(elves))

	fmt.Println(elves[len(elves)-1])
	fmt.Println(elves[len(elves)-1] + elves[len(elves)-2] + elves[len(elves)-3])

}
