package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer func(fp *os.File) {
		_ = fp.Close()
	}(fp)

	stacks := make([][]string, 9)
	stacks2 := make([][]string, 9)

	br := bufio.NewReader(fp)
	for {
		line, _, err := br.ReadLine()
		if err != nil {
			fmt.Println(err)
			break
		}
		if strings.HasPrefix(string(line), " 1") {
			_, _, _ = br.ReadLine()
			break
		}
		for i := 0; i < len(line); i = i + 4 {
			if line[i] == '[' {
				stacks[i/4] = append(stacks[i/4], string(line[i+1]))
			}
		}
	}

	for i, s := range stacks {
		var inverse []string
		for _, r := range s {
			inverse = append([]string{r}, inverse...)
		}
		stacks[i] = inverse
		for _, j := range inverse {
			stacks2[i] = append(stacks2[i], j)
		}
	}

	for {
		line, _, err := br.ReadLine()
		if err != nil {
			fmt.Println(err)
			break
		}
		var num, from, to int
		_, err = fmt.Sscanf(string(line), "move %d from %d to %d", &num, &from, &to)
		from--
		to--
		if err != nil {
			fmt.Println(err)
			break
		}

		for i := 0; i < num; i++ {
			fromLength := len(stacks[from])
			stacks[to] = append(stacks[to], stacks[from][fromLength-1])
			stacks[from] = stacks[from][:fromLength-1]
		}
		//Part2
		l := len(stacks2[from])
		stacks2[to] = append(stacks2[to], stacks2[from][l-num:]...)
		stacks2[from] = stacks2[from][:l-num]
	}

	for _, stack := range stacks {
		fmt.Print(stack[len(stack)-1])
	}
	fmt.Println()

	for _, stack := range stacks2 {
		fmt.Print(stack[len(stack)-1])
	}
	fmt.Println()
}
