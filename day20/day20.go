package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

type Node struct {
	value int
	prev  *Node
	next  *Node
}

func main() {
	fp, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()
	scanner := bufio.NewScanner(fp)

	var input []int

	for scanner.Scan() {
		i, err := strconv.Atoi(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}
		input = append(input, i)
	}

	factor := 811589153

	var zeroNode *Node
	nodes := make([]*Node, 0, len(input))
	for idx, i := range input {
		n := Node{value: i * factor}
		if i == 0 {
			zeroNode = &n
		}
		if idx > 0 {
			n.prev = nodes[idx-1]
			nodes[idx-1].next = &n
		}
		nodes = append(nodes, &n)
	}
	nodes[len(nodes)-1].next = nodes[0]
	nodes[0].prev = nodes[len(nodes)-1]

	start := time.Now()
	for i := 0; i < 10; i++ {
		for _, node := range nodes {
			if node.value == 0 {
				continue
			}
			// remove node from its position
			node.prev.next = node.next
			node.next.prev = node.prev

			remainder := node.value % (len(nodes) - 1)

			current := node
			if node.value > 0 {
				for i := 0; i < remainder; i++ {
					current = current.next
				}
			} else {
				for i := 0; i >= remainder; i-- {
					current = current.prev
				}
			}
			newNext := current.next

			current.next = node
			node.prev = current
			newNext.prev = node
			node.next = newNext
		}
		fmt.Println(i)
	}

	pos := zeroNode
	sum := 0
	for i := 1; i <= 3000; i++ {
		pos = pos.next
		if i%1000 == 0 {
			fmt.Println(i, pos.value)
			sum += pos.value
		}
	}

	end := time.Now()
	fmt.Println(end.Sub(start), sum)
}
