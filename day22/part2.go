package main

import "log"

func wrapPartII(current tile, facing int, tilesByFace [7]map[vec2]tile, facesSize int) (nextTile tile, nextFacing int) {
	currentFace := current.face
	// determine the next face we end up on
	//nextFace := testWrap(currentFace, facing)
	//nextFacing = testTransitions(currentFace, nextFace, facing)
	nextFace := inputWrap(currentFace, facing)
	nextFacing = inputTransitions(currentFace, nextFace, facing)

	nextFaceRow, nextFaceCol := coordWrap(current.fRow, current.fCol, facing, nextFacing, facesSize)

	if _, notEmpty := tilesByFace[nextFace][vec2{nextFaceRow, nextFaceCol}]; !notEmpty {
		log.Fatal("empty tile after wrap")
	}

	return tilesByFace[nextFace][vec2{nextFaceRow, nextFaceCol}], nextFacing
}

func testFaces(row, col int) int {
	switch {
	case (row-1)/4 == 0 && (col-1)/4 == 2:
		return 1
	case (row-1)/4 == 1 && (col-1)/4 == 0:
		return 2
	case (row-1)/4 == 1 && (col-1)/4 == 1:
		return 3
	case (row-1)/4 == 1 && (col-1)/4 == 2:
		return 4
	case (row-1)/4 == 2 && (col-1)/4 == 2:
		return 5
	case (row-1)/4 == 2 && (col-1)/4 == 3:
		return 6
	}
	log.Fatal("unknown face")
	return 0
}

func inputFaces(row, col int) (face int) {
	faceSize := 50
	switch {
	case (row-1)/faceSize == 0 && (col-1)/faceSize == 1:
		return 1
	case (row-1)/faceSize == 0 && (col-1)/faceSize == 2:
		return 2
	case (row-1)/faceSize == 1 && (col-1)/faceSize == 1:
		return 3
	case (row-1)/faceSize == 2 && (col-1)/faceSize == 0:
		return 4
	case (row-1)/faceSize == 2 && (col-1)/faceSize == 1:
		return 5
	case (row-1)/faceSize == 3 && (col-1)/faceSize == 0:
		return 6
	}
	log.Fatal("unknown face")
	return 0
}

func testWrap(face, facing int) (newFace int) {
	switch {
	case face == 1 && facing == 0:
		return 6
	case face == 1 && facing == 2:
		return 3
	case face == 1 && facing == 3:
		return 2

	case face == 2 && facing == 1:
		return 5
	case face == 2 && facing == 2:
		return 6
	case face == 2 && facing == 3:
		return 1

	case face == 3 && facing == 1:
		return 5
	case face == 3 && facing == 3:
		return 1

	case face == 4 && facing == 0:
		return 6

	case face == 5 && facing == 1:
		return 2
	case face == 5 && facing == 2:
		return 3

	case face == 6 && facing == 0:
		return 1
	case face == 6 && facing == 1:
		return 2
	case face == 6 && facing == 3:
		return 4
	}
	log.Fatal("unknown wrap")
	return 0
}

func inputWrap(face, facing int) (newFace int) {
	switch {
	case face == 1 && facing == 2:
		return 4
	case face == 1 && facing == 3:
		return 6

	case face == 2 && facing == 0:
		return 5
	case face == 2 && facing == 1:
		return 3
	case face == 2 && facing == 3:
		return 6

	case face == 3 && facing == 0:
		return 2
	case face == 3 && facing == 2:
		return 4

	case face == 4 && facing == 2:
		return 1
	case face == 4 && facing == 3:
		return 3

	case face == 5 && facing == 0:
		return 2
	case face == 5 && facing == 1:
		return 6

	case face == 6 && facing == 0:
		return 5
	case face == 6 && facing == 1:
		return 2
	case face == 6 && facing == 2:
		return 1
	}
	log.Fatal("unknown wrap")
	return 0
}

func inputTransitions(from, to, facing int) (newFacing int) {
	switch {
	case from == to:
		return facing

	case from == 1 && to == 2:
		return 0
	case from == 1 && to == 3:
		return 1
	case from == 1 && to == 4:
		return 0
	case from == 1 && to == 6:
		return 0

	case from == 2 && to == 1:
		return 2
	case from == 2 && to == 3:
		return 2
	case from == 2 && to == 5:
		return 2
	case from == 2 && to == 6:
		return 3

	case from == 3 && to == 1:
		return 3
	case from == 3 && to == 2:
		return 3
	case from == 3 && to == 4:
		return 1
	case from == 3 && to == 5:
		return 1

	case from == 4 && to == 1:
		return 0
	case from == 4 && to == 3:
		return 0
	case from == 4 && to == 5:
		return 0
	case from == 4 && to == 6:
		return 1

	case from == 5 && to == 2:
		return 2
	case from == 5 && to == 3:
		return 3
	case from == 5 && to == 4:
		return 2
	case from == 5 && to == 6:
		return 2

	case from == 6 && to == 1:
		return 1
	case from == 6 && to == 2:
		return 1
	case from == 6 && to == 4:
		return 3
	case from == 6 && to == 5:
		return 3
	}
	log.Fatal("unknown transition")
	return 0
}

func testTransitions(from, to, facing int) (newFacing int) {
	switch {
	case from == to:
		return facing

	case from == 1 && to == 2:
		return 1 // 3 -> 1
	case from == 1 && to == 3:
		return 1 // 2 -> 1
	case from == 1 && to == 4:
		return 1 // 1 -> 1
	case from == 1 && to == 6:
		return 2 // 0 -> 2

	case from == 2 && to == 1:
		return 1 // 3 -> 1
	case from == 2 && to == 3:
		return 0 // 0 -> 0
	case from == 2 && to == 5:
		return 3 // 1 -> 3
	case from == 2 && to == 6:
		return 3 // 2 -> 3

	case from == 3 && to == 1:
		return 0 // 3 -> 0
	case from == 3 && to == 2:
		return 2 // 2 -> 2
	case from == 3 && to == 4:
		return 0 // 0 -> 0
	case from == 3 && to == 5:
		return 2 // 1 -> 2

	case from == 4 && to == 1:
		return 3 // 3 -> 3
	case from == 4 && to == 3:
		return 2 // 2 -> 2
	case from == 4 && to == 5:
		return 1 // 1 -> 1
	case from == 4 && to == 6:
		return 1 // 0 -> 1

	case from == 5 && to == 2:
		return 3 // 1 -> 3
	case from == 5 && to == 3:
		return 3 // 2 -> 3
	case from == 5 && to == 4:
		return 3 // 3 -> 3
	case from == 5 && to == 6:
		return 0 // 0 -> 0

	case from == 6 && to == 1:
		return 2 // 0 -> 2
	case from == 6 && to == 2:
		return 0 // 1 -> 0
	case from == 6 && to == 4:
		return 2 // 3 -> 2
	case from == 6 && to == 5:
		return 2 // 2 -> 2
	}
	log.Fatal("unknown transition")
	return 0
}
