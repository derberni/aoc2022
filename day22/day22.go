package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type tile struct {
	row  int
	col  int
	wall bool
	face int
	fRow int
	fCol int
}

func (t tile) position() vec2 {
	return vec2{t.row, t.col}
}

type vec2 struct {
	row int
	col int
}

func (v vec2) add(o vec2) vec2 {
	return vec2{v.row + o.row, v.col + o.col}
}

func (v vec2) mod(o vec2) vec2 {
	return vec2{v.row % o.row, v.col % o.col}
}

func wrapPartI(current tile, facing int, tiles map[vec2]tile, maxPos vec2) (nextTile tile, nextFacing int) {
	nextPos := current.position().add(directions[facing]).add(maxPos).mod(maxPos)
	for {
		if _, notEmpty := tiles[nextPos]; notEmpty {
			break
		} else {
			nextPos = nextPos.add(directions[facing]).add(maxPos).mod(maxPos)
		}
	}
	return tiles[nextPos], facing
}

func coordWrap(row, col int, fromFacing, toFacing int, faceSize int) (newRow, newCol int) {
	if fromFacing == 0 {
		switch toFacing {
		case 0:
			return row, 1
		case 1:
			return 1, faceSize - row + 1
		case 2:
			return faceSize - row + 1, faceSize
		case 3:
			return faceSize, row
		}
	} else if fromFacing == 1 {
		switch toFacing {
		case 0:
			return faceSize - col + 1, 1
		case 1:
			return 1, col
		case 2:
			return col, faceSize
		case 3:
			return faceSize, faceSize - col + 1
		}
	} else if fromFacing == 2 {
		switch toFacing {
		case 0:
			return faceSize - row + 1, 1
		case 1:
			return 1, row
		case 2:
			return row, faceSize
		case 3:
			return faceSize, faceSize - row + 1
		}
	} else if fromFacing == 3 {
		switch toFacing {
		case 0:
			return col, 1
		case 1:
			return 1, faceSize - col + 1
		case 2:
			return faceSize - col + 1, faceSize
		case 3:
			return faceSize, col
		}
	}
	log.Fatal("invalid coord wrap")
	return 0, 0
}

// 0: right, 1: down, 2: left, 3: up
var directions = map[int]vec2{0: {0, 1}, 1: {1, 0}, 2: {0, -1}, 3: {-1, 0}}

func main() {
	input := "input"
	part2 := true
	fp, err := os.Open(input)
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	var facesFn func(row, col int) (face int)
	//var transitionFn func(int, int, int) int
	var facesSize int
	if input == "testinput" {
		facesFn = testFaces
		facesSize = 4
	} else {
		facesFn = inputFaces
		facesSize = 50
	}

	scanner := bufio.NewScanner(fp)

	tiles := map[vec2]tile{}
	tilesByFace := [7]map[vec2]tile{{}, {}, {}, {}, {}, {}, {}}
	//for i := 0; i < 6; i++ {
	//	tilesByFace[i] = map[vec2]tile{}
	//}
	maxCols := 0

	row := 0
	for scanner.Scan() {
		faceRow := (row % facesSize) + 1
		row++
		line := scanner.Text()
		if line == "" {
			break
		}
		for col, r := range line {
			faceCol := (col % facesSize) + 1
			col++ // indexing starts at 1

			if r == '.' {
				face := facesFn(row, col)
				t := tile{row, col, false, face, faceRow, faceCol}
				tiles[vec2{row, col}] = t
				tilesByFace[face][vec2{faceRow, faceCol}] = t
			} else if r == '#' {
				face := facesFn(row, col)
				t := tile{row, col, true, face, faceRow, faceCol}
				tiles[vec2{row, col}] = t
				tilesByFace[face][vec2{faceRow, faceCol}] = t
			}
			if col > maxCols {
				maxCols = col
			}
		}

	}
	scanner.Scan()
	line := scanner.Text()

	maxPos := vec2{row + 1, maxCols + 1}

	//for row := 1; row <= maxPos.row; row++ {
	//	for col := 1; col <= maxPos.col; col++ {
	//		t, notEmpty := tiles[vec2{row, col}]
	//		if !notEmpty {
	//			fmt.Print(" ")
	//		} else {
	//			if t.wall {
	//				fmt.Print("#")
	//			} else {
	//				fmt.Print(".")
	//			}
	//		}
	//	}
	//	fmt.Print("\n")
	//}

	facing := 0
	position := vec2{1, 1}
	for {
		// loop until start position found
		if t, ok := tiles[position]; ok && !t.wall {
			break
		}
		position = position.add(directions[facing])
	}

	// main loop
	for {
		var steps int
		_, err := fmt.Sscan(line, &steps)
		if err != nil {
			log.Fatal(err)
		}
		line = line[len(fmt.Sprint(steps)):]

		nextFacing := facing
		for i := 0; i < steps; i++ {
			var nextTile tile
			// check if there is a tile at the next position
			if nt, notEmpty := tiles[position.add(directions[facing])]; notEmpty {
				// tile found
				nextTile = nt
			} else {
				// we need to wrap around
				if part2 {
					nextTile, nextFacing = wrapPartII(tiles[position], facing, tilesByFace, facesSize)
				} else {
					nextTile, nextFacing = wrapPartI(tiles[position], facing, tiles, maxPos)
				}
			}

			if nextTile.wall {
				break
			}
			// if we did not hit a wall, update position
			position = nextTile.position()
			facing = nextFacing
		}

		if len(line) == 0 {
			break
		}
		var turn byte
		turn, line = line[0], line[1:]

		if turn == 'R' {
			facing = (facing + 1) % 4
		} else if turn == 'L' {
			facing = (facing + 3) % 4
		} else {
			log.Fatal("invalid turn direction")
		}

	}
	// r d r d r d r
	fmt.Println(1000*position.row + 4*position.col + facing)

}
