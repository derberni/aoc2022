package main

import (
	"log"
	"strconv"
)

func NewParser(l *Lexer) *Parser {
	p := &Parser{l: l, signalStrength: []int{1}, cycle: 1}

	p.nextToken()
	p.nextToken()

	return p
}

type Parser struct {
	l *Lexer

	curToken  Token
	peekToken Token

	signalStrength []int
	cycle          int
}

func (p *Parser) nextToken() {
	p.curToken = p.peekToken
	p.peekToken = p.l.NextToken()
	if p.peekTokenIs(ILLEGAL) {
		log.Fatal("illegal token encountered", p.peekToken.Literal)
	}
}

func (p *Parser) ParseInstructions() []int {

	for !p.peekTokenIs(EOF) {
		p.parseInstruction()
	}

	return p.signalStrength
}

func (p *Parser) curTokenIs(tokenType TokenType) bool {
	return p.curToken.Type == tokenType
}

func (p *Parser) peekTokenIs(tokenType TokenType) bool {
	return p.peekToken.Type == tokenType
}

func (p *Parser) parseInstruction() {
	switch p.curToken.Type {
	case NOOP:
		p.parseNOOP()
	case ADDX:
		p.parseADDX()
	}

}

func (p *Parser) parseADDX() {
	p.signalStrength = append(p.signalStrength, p.signalStrength[p.cycle-1])
	p.cycle++
	p.nextToken()

	addVal, err := strconv.Atoi(p.curToken.Literal)
	if err != nil {
		log.Fatal(err)
	}
	p.signalStrength = append(p.signalStrength, p.signalStrength[p.cycle-1]+addVal)
	p.cycle++
	p.nextToken()
}

func (p *Parser) parseNOOP() {
	p.signalStrength = append(p.signalStrength, p.signalStrength[p.cycle-1])
	p.cycle++
	p.nextToken()
}
