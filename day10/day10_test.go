package main

import (
	"io"
	"os"
	"testing"
)

var smallInput = "noop\naddx 3\naddx -5"

func min2(x, y int) int {
	if x <= y {
		return x
	}
	return y
}

func max2(x, y int) int {
	if x >= y {
		return x
	}
	return y
}

func TestMain_small(t *testing.T) {
	l := NewLexer(smallInput)

	p := NewParser(l)

	signalStrength := p.ParseInstructions()

	t.Log(signalStrength)
}

func TestMain_main(t *testing.T) {
	fp, err := os.Open("testinput")
	if err != nil {
		t.Fatal(err)
	}
	defer fp.Close()

	input, err := io.ReadAll(fp)
	if err != nil {
		t.Fatal(err)
	}

	l := NewLexer(string(input))

	p := NewParser(l)

	signalStrength := p.ParseInstructions()

	tt := []struct {
		index int
		value int
	}{{20, 21}, {60, 19}, {100, 18}, {140, 21}, {180, 16}, {220, 18}}

	if len(signalStrength) < tt[len(tt)-1].index {
		t.Fatalf("not enough values in signal strength: got %d, want %d", len(signalStrength), tt[len(tt)-1].index)
	}

	for _, tc := range tt {
		if signalStrength[tc.index-1] != tc.value {
			t.Errorf("index %d: wrong signal strength, got %d, want %d (%v)", tc.index, signalStrength[tc.index-1], tc.value, signalStrength[max2(tc.index-3, 0):min2(tc.index+3, len(signalStrength)-1)])
		}
	}
}
