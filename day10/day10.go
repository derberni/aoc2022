package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	input, err := io.ReadAll(fp)
	if err != nil {
		log.Fatal(err)
	}

	l := NewLexer(string(input))

	p := NewParser(l)

	signalStrength := p.ParseInstructions()

	indices := []int{20, 60, 100, 140, 180, 220}

	sum := 0

	for _, i := range indices {
		sum += i * signalStrength[i-1]
	}

	fmt.Println(sum)

	for i := range signalStrength {
		spriteMin := signalStrength[i] - 1
		spriteMax := signalStrength[i] + 1
		pixelPosition := i % 40
		if pixelPosition == 0 {
			fmt.Print("\n")
		}
		if pixelPosition >= spriteMin && pixelPosition <= spriteMax {
			fmt.Print("#")
		} else {
			fmt.Print(".")
		}
	}
}
