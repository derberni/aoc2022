package main

var Monkeys = []Monkey{
	{
		ID:    0,
		Items: []int{63, 57},
		Operation: func(old int) int {
			return old * 11
		},
		Test: func(i int) int {
			if i%7 == 0 {
				return 6
			}
			return 2
		},
	},
	{
		ID:    1,
		Items: []int{82, 66, 87, 78, 77, 92, 83},
		Operation: func(old int) int {
			return old + 1
		},
		Test: func(i int) int {
			if i%11 == 0 {
				return 5
			}
			return 0
		},
	},
	{
		ID:    2,
		Items: []int{97, 53, 53, 85, 58, 54},
		Operation: func(old int) int {
			return old * 7
		},
		Test: func(i int) int {
			if i%13 == 0 {
				return 4
			}
			return 3
		},
	},
	{
		ID:    3,
		Items: []int{50},
		Operation: func(old int) int {
			return old + 3
		},
		Test: func(i int) int {
			if i%3 == 0 {
				return 1
			}
			return 7
		},
	},
	{
		ID:    4,
		Items: []int{64, 69, 52, 65, 73},
		Operation: func(old int) int {
			return old + 6
		},
		Test: func(i int) int {
			if i%17 == 0 {
				return 3
			}
			return 7
		},
	},
	{
		ID:    5,
		Items: []int{57, 91, 65},
		Operation: func(old int) int {
			return old + 5
		},
		Test: func(i int) int {
			if i%2 == 0 {
				return 0
			}
			return 6
		},
	},
	{
		ID:    6,
		Items: []int{67, 91, 84, 78, 60, 69, 99, 83},
		Operation: func(old int) int {
			return old * old
		},
		Test: func(i int) int {
			if i%5 == 0 {
				return 2
			}
			return 4
		},
	},
	{
		ID:    7,
		Items: []int{58, 78, 69, 65},
		Operation: func(old int) int {
			return old + 7
		},
		Test: func(i int) int {
			if i%19 == 0 {
				return 5
			}
			return 1
		},
	},
}
