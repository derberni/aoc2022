package main

var TestMonkeys = []Monkey{
	{
		ID:    0,
		Items: []int{79, 98},
		Operation: func(old int) int {
			return old * 19
		},
		Test: func(i int) int {
			if i%23 == 0 {
				return 2
			}
			return 3
		},
	},
	{
		ID:    1,
		Items: []int{54, 65, 75, 74},
		Operation: func(old int) int {
			return old + 6
		},
		Test: func(i int) int {
			if i%19 == 0 {
				return 2
			}
			return 0
		},
	},
	{
		ID:    2,
		Items: []int{79, 60, 97},
		Operation: func(old int) int {
			return old * old
		},
		Test: func(i int) int {
			if i%13 == 0 {
				return 1
			}
			return 3
		},
	},
	{
		ID:    3,
		Items: []int{74},
		Operation: func(old int) int {
			return old + 3
		},
		Test: func(i int) int {
			if i%17 == 0 {
				return 0
			}
			return 1
		},
	},
}
