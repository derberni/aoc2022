package main

import (
	"fmt"
	"sort"
)

type Monkey struct {
	ID          int
	Items       []int
	Operation   func(int) int
	Test        func(int) int
	Inspections int
}

func main() {

	monkeys := Monkeys
	inspections := make([]int, len(monkeys))
	for round := 0; round < 10000; round++ {
		for _, monkey := range monkeys {
			for _, item := range monkey.Items {
				item = monkey.Operation(item)
				//item = int(math.Floor(float64(item) / float64(3)))
				item = item % (7 * 11 * 13 * 3 * 17 * 2 * 5 * 19)
				target := monkey.Test(item)
				monkeys[target].Items = append(monkeys[target].Items, item)
				inspections[monkey.ID]++
			}
			monkeys[monkey.ID].Items = nil
		}
	}

	fmt.Println(inspections)

	sort.Sort(sort.IntSlice(inspections))

	fmt.Println(inspections[len(inspections)-1] * inspections[len(inspections)-2])

}
