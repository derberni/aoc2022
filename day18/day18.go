package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type cube struct {
	x int
	y int
	z int
}

type face struct {
	x           int
	y           int
	z           int
	orientation int // 0 == xy plane, 1 == xz plane, 2 == yx plane
}

func getFaces(x, y, z int) map[face]bool {
	faces := map[face]bool{}
	faces[face{x, y, z, 0}] = true
	faces[face{x, y, z + 1, 0}] = true
	faces[face{x, y, z, 1}] = true
	faces[face{x, y + 1, z, 1}] = true
	faces[face{x, y, z, 2}] = true
	faces[face{x + 1, y, z, 2}] = true

	return faces
}

func getBBox(faces map[face]bool) (cube, cube) {
	var bboxMin, bboxMax cube

	for f := range faces {
		if f.x > bboxMax.x {
			bboxMax.x = f.x
		}
		if f.y > bboxMax.y {
			bboxMax.y = f.y
		}
		if f.z > bboxMax.z {
			bboxMax.z = f.z
		}
		if f.x < bboxMin.x {
			bboxMin.x = f.x
		}
		if f.y < bboxMin.y {
			bboxMin.y = f.y
		}
		if f.z < bboxMin.z {
			bboxMin.z = f.z
		}
	}

	bboxMin.x -= 2
	bboxMin.y -= 2
	bboxMin.z -= 2
	bboxMax.x += 2
	bboxMax.y += 2
	bboxMax.z += 2
	return bboxMin, bboxMax
}

func neighbors(x, y, z int) map[cube]bool {
	n := map[cube]bool{}
	n[cube{x - 1, y, z}] = true
	n[cube{x + 1, y, z}] = true
	n[cube{x, y - 1, z}] = true
	n[cube{x, y + 1, z}] = true
	n[cube{x, y, z - 1}] = true
	n[cube{x, y, z + 1}] = true
	return n
}

func main() {
	fp, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()
	scanner := bufio.NewScanner(fp)

	faces := map[face]bool{}
	cubes := map[cube]bool{}

	start1 := time.Now()
	for scanner.Scan() {
		c := make([]int, 3)
		for i, v := range strings.Split(scanner.Text(), ",") {
			c[i], err = strconv.Atoi(v)
			if err != nil {
				log.Fatal(err)
			}
		}
		cubes[cube{c[0], c[1], c[2]}] = true
		for f := range getFaces(c[0], c[1], c[2]) {
			if ok := faces[f]; ok {
				delete(faces, f)
			} else {
				faces[f] = true
			}
		}

	}
	end1 := time.Now()
	fmt.Println(end1.Sub(start1), len(faces))

	start2 := time.Now()
	boxMin, boxMax := getBBox(faces)

	steamCubes := map[cube]bool{}
	intersectionFaces := map[face]bool{}
	steamCubes[boxMin] = true
	for f := range getFaces(boxMin.x, boxMin.y, boxMin.z) {
		intersectionFaces[f] = true
	}

	for z := boxMin.z; z < boxMax.z; z++ {
		for x := boxMin.x; x < boxMax.x; x++ {
			for y := boxMin.y; y < boxMax.y; y++ {
				if x == boxMin.x && y == boxMin.y && z == boxMin.z {
					continue
				}
				if cubes[cube{x, y, z}] {
					// cube is filled with lava
					continue
				}
				hasNeighbor := false
				for n := range neighbors(x, y, z) {
					if steamCubes[n] {
						hasNeighbor = true
						break
					}
				}
				if hasNeighbor {
					steamCubes[cube{x, y, z}] = true
					for f := range getFaces(x, y, z) {
						if ok := intersectionFaces[f]; ok {
							delete(intersectionFaces, f)
						} else {
							intersectionFaces[f] = true
						}
					}
				}
			}
		}
	}
	// fill again from the top
	for z := boxMax.z - 1; z >= boxMin.z; z-- {
		for x := boxMax.x - 1; x >= boxMin.x; x-- {
			for y := boxMax.y - 1; y >= boxMin.y; y-- {
				if x == boxMin.x && y == boxMin.y && z == boxMin.z {
					continue
				}
				if cubes[cube{x, y, z}] || steamCubes[cube{x, y, z}] {
					// cube is filled with lava
					continue
				}
				hasNeighbor := false
				for n := range neighbors(x, y, z) {
					if steamCubes[n] {
						hasNeighbor = true
						break
					}
				}
				if hasNeighbor {
					steamCubes[cube{x, y, z}] = true
					for f := range getFaces(x, y, z) {
						if ok := intersectionFaces[f]; ok {
							delete(intersectionFaces, f)
						} else {
							intersectionFaces[f] = true
						}
					}
				}
			}
		}
	}

	end2 := time.Now()
	fmt.Println(end2.Sub(start2), len(intersectionFaces)-2*(boxMax.x-boxMin.x)*(boxMax.y-boxMin.y)-2*(boxMax.x-boxMin.x)*(boxMax.z-boxMin.z)-2*(boxMax.z-boxMin.z)*(boxMax.y-boxMin.y))
}
