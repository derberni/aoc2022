package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
)

func bbox[V any](m map[util.Vec2]V) (util.Vec2, util.Vec2) {
	min := util.Vec2{X: math.MaxInt, Y: math.MaxInt}
	max := util.Vec2{X: math.MinInt, Y: math.MinInt}

	for el := range m {
		if el.X < min.X {
			min.X = el.X
		}
		if el.Y < min.Y {
			min.Y = el.Y
		}
		if el.X > max.X {
			max.X = el.X
		}
		if el.Y > max.Y {
			max.Y = el.Y
		}
	}

	return min, max
}

func dumpElves(elves map[util.Vec2]bool) {
	min, max := bbox(elves)
	for y := 0; y < max.Y-min.Y+1; y++ {
		for x := 0; x < max.X-min.X+1; x++ {
			if elves[util.Vec2{X: min.X + x, Y: min.Y + y}] {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Print("\n")
	}
}

// N S W E
var directions = map[int]util.Vec2{0: {0, -1}, 1: {0, 1}, 2: {-1, 0}, 3: {1, 0}}

func main() {
	input := "testinput"

	fp, err := os.Open(input)
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)

	elves := map[util.Vec2]bool{}

	y := 0
	for scanner.Scan() {
		for x, v := range scanner.Text() {
			if v == '#' {
				elves[util.Vec2{Y: y, X: x}] = true
			}
		}
		y++
	}

	startMin, startMax := bbox(elves)
	fmt.Println(startMin, startMax)

	L := 1000

	currentDirection := 0
	for step := 0; step < L; step++ {
		//fmt.Println("step", step)
		//dumpElves(elves)
		//fmt.Println()

		// determine elves that need to move
		elvesMoveQ := map[util.Vec2]bool{}
		for e := range elves {
			if _, ok := elvesMoveQ[e]; ok {
				continue
			}
			for n := range e.Neighbors8() {
				if otherElf := elves[n]; otherElf {
					elvesMoveQ[n] = true
					elvesMoveQ[e] = true
					break
				}
			}
		}
		if len(elvesMoveQ) == 0 {
			fmt.Println("no elves to move", step)
			break
		}
		// get targets for elves
		moveTarget := map[util.Vec2][]util.Vec2{}
		for e := range elvesMoveQ {
			elfDirection := currentDirection

			for i := 0; i < 4; i++ {
				var neighbors map[util.Vec2]bool
				switch elfDirection {
				case 0:
					neighbors = e.NeighborsNorth()
				case 1:
					neighbors = e.NeighborsSouth()
				case 2:
					neighbors = e.NeighborsWest()
				case 3:
					neighbors = e.NeighborsEast()
				}
				if util.MapAny(elves, neighbors) {
					elfDirection = (elfDirection + 1) % 4
					continue
				}
				moveTarget[e.Add(directions[elfDirection])] = append(moveTarget[e.Add(directions[elfDirection])], e)
				break
			}
		}
		currentDirection = (currentDirection + 1) % 4
		// time to move
		elvesCouldMove := 0
		for target, el := range moveTarget {
			// only move if the target only contains one elf
			if len(el) == 1 {
				elvesCouldMove++
				elves[target] = true
				delete(elves, el[0])
			}
		}
		if elvesCouldMove == 0 {
			fmt.Println("no elves could move anymore", step)
		}
	}

	endMin, endMax := bbox(elves)
	fmt.Println(endMin, endMax)
	fmt.Println((endMax.X-endMin.X+1)*(endMax.Y-endMin.Y+1) - len(elves))
}
