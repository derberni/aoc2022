package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"
)

func TestMain_getFullyConnected(t *testing.T) {
	fp, err := os.Open("testinput")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)

	valves := make(map[string]int)

	tunnels := make(map[string][]string)

	for scanner.Scan() {
		line := scanner.Text()
		var v, discard string
		var t []string
		var fr int
		_, err = fmt.Sscanf(line, "Valve %s has flow rate=%d; %s", &v, &fr, &discard)
		if err != nil {
			log.Fatal(err)
		}
		if discard == "tunnel" {
			i := strings.LastIndex(line, "tunnel leads to valve")
			tmp := strings.TrimSpace(line[i+len("tunnel leads to valve")+1:])

			t = append(t, tmp)
		} else {
			i := strings.LastIndex(line, "tunnels lead to valves")
			tmp := strings.Split(line[i+len("tunnels lead to valves")+1:], ",")
			for _, tmpI := range tmp {
				t = append(t, strings.TrimSpace(tmpI))
			}

		}
		valves[v] = fr
		tunnels[v] = append(tunnels[v], t...)
	}

	nodes := getFullyConnected(valves, tunnels)

	fmt.Println(nodes)

}
