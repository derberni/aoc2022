package main

import (
	"bufio"
	"fmt"
	"golang.org/x/exp/slices"
	"log"
	"math"
	"os"
	"sort"
	"strings"
	"time"
)

type ValveNode struct {
	id        string
	flow      int
	distances map[string]int
}

type memoItem struct {
	remaining   int
	flow        int
	alreadyOpen string
}

func (mi memoItem) Hash() string {
	open := strings.Split(strings.Trim(mi.alreadyOpen, ","), ",")
	sort.Sort(sort.StringSlice(open))
	return fmt.Sprint(mi.remaining, mi.flow, strings.Join(open, ","))
}

func shortesPath(start string, valves map[string]int, tunnels map[string][]string) map[string]int {
	q := map[string]bool{}
	dist := map[string]int{}

	for v := range valves {
		q[v] = true
		dist[v] = math.MaxInt
	}
	dist[start] = 0

	for len(q) > 0 {
		i := ""
		minDist := math.MaxInt
		for next := range q {
			if nextDist := dist[next]; nextDist < minDist {
				minDist = nextDist
				i = next
			}
		}
		delete(q, i)
		for neigh := range q {
			isNeighbor := slices.Contains(tunnels[i], neigh)
			if !isNeighbor {
				continue
			}
			tmp := minDist + 1
			if tmp < dist[neigh] {
				dist[neigh] = tmp
			}
		}
	}
	return dist
}

func getFullyConnected(valves map[string]int, tunnels map[string][]string) map[string]ValveNode {
	nodes := map[string]ValveNode{}
	for source := range valves {
		if valves[source] == 0 && source != "AA" {
			continue
		}
		dist := shortesPath(source, valves, tunnels)
		distanceNonzero := map[string]int{}
		for target, d := range dist {
			if valves[target] == 0 || target == source {
				continue
			}
			distanceNonzero[target] = d
		}
		nodes[source] = ValveNode{id: source, flow: valves[source], distances: distanceNonzero}
	}
	return nodes
}

func min2(x, y int) int {
	if x <= y {
		return x
	}
	return y
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)

	valves := make(map[string]int)
	tunnels := make(map[string][]string)

	for scanner.Scan() {
		line := scanner.Text()
		var v, discard string
		var t []string
		var fr int
		_, err = fmt.Sscanf(line, "Valve %s has flow rate=%d; %s", &v, &fr, &discard)
		if err != nil {
			log.Fatal(err)
		}
		if discard == "tunnel" {
			i := strings.LastIndex(line, "tunnel leads to valve")
			tmp := strings.TrimSpace(line[i+len("tunnel leads to valve")+1:])

			t = append(t, tmp)
		} else {
			i := strings.LastIndex(line, "tunnels lead to valves")
			tmp := strings.Split(line[i+len("tunnels lead to valves")+1:], ",")
			for _, tmpI := range tmp {
				t = append(t, strings.TrimSpace(tmpI))
			}

		}
		valves[v] = fr
		tunnels[v] = append(tunnels[v], t...)
	}

	nodes := getFullyConnected(valves, tunnels)

	//memo := map[string]memoItem{}

	var elephant func(current, currentElephant string, remaining, remainingElephant int, minutesRemaining int, open string) (int, string)
	elephant = func(current, currentElephant string, remaining, remainingElephant int, minutesRemaining int, open string) (int, string) {
		if minutesRemaining <= 0 {
			return 0, open
		}

		if remaining < remainingElephant {
			if current == "" {
				return 0, open
			}
			// we reach the next valve before the elephant
			timeSpent := 0
			openPressure := 0
			if current != "AA" {
				timeSpent = 1
				openPressure += nodes[current].flow * (minutesRemaining - 1)
				// handle case where opening the current valve is best case
				open += "," + current
			}

			newOpen := open
			subPressure := 0
			movedOn := false
			for n, d := range nodes[current].distances {
				if d > minutesRemaining-timeSpent || strings.Contains(open, n) || n == currentElephant {
					continue
				}
				nextArrival := min2(d+timeSpent, remainingElephant)
				my, myOpen := elephant(n, currentElephant, d+timeSpent-nextArrival, remainingElephant-nextArrival, minutesRemaining-nextArrival, open)
				if my > subPressure {
					subPressure = my
					newOpen = myOpen
				}
				movedOn = true
			}
			if !movedOn && currentElephant != "" {
				subPressure, newOpen = elephant("", currentElephant, math.MaxInt, 0, minutesRemaining-remainingElephant, open)
			}

			return subPressure + openPressure, newOpen
		} else if remainingElephant < remaining {
			if currentElephant == "" {
				return 0, open
			}
			timeSpent := 0
			openPressure := 0
			if currentElephant != "AA" {
				timeSpent = 1
				openPressure += nodes[currentElephant].flow * (minutesRemaining - 1)
				// handle case where opening the current valve is best case
				open += "," + currentElephant
			}

			newOpen := open
			subPressure := 0
			movedOn := false
			for n, d := range nodes[currentElephant].distances {
				if d > minutesRemaining-timeSpent || strings.Contains(open, n) || n == current {
					continue
				}
				nextArrival := min2(d+timeSpent, remaining)
				my, myOpen := elephant(current, n, remaining-nextArrival, d+timeSpent-nextArrival, minutesRemaining-nextArrival, open)
				if my > subPressure {
					subPressure = my
					newOpen = myOpen
				}
				movedOn = true
			}
			if !movedOn && current != "" {
				subPressure, newOpen = elephant(current, "", 0, math.MaxInt, minutesRemaining-remaining, open)
			}

			return subPressure + openPressure, newOpen
		}

		timeSpent := 0
		openPressure := 0
		if current != "AA" {
			timeSpent = 1
			openPressure += nodes[current].flow * (minutesRemaining - 1)
			// handle case where opening the current valve is best case
			open += "," + current
		}

		openPressureE := 0
		if currentElephant != "AA" {
			openPressureE += nodes[currentElephant].flow * (minutesRemaining - 1)
			// handle case where opening the current valve is best case
			open += "," + currentElephant
		}

		newOpen := open
		subPressure := 0

		for key := range nodes {
			if key == "AA" || strings.Contains(open, key) {
				continue
			}
			for key2 := range nodes {
				if key == "AA" || strings.Contains(open, key2) || key == key2 {
					continue
				}
				selfDist := nodes[current].distances[key]
				elephantDist := nodes[currentElephant].distances[key]
				selfDist2 := nodes[current].distances[key2]
				elephantDist2 := nodes[currentElephant].distances[key2]

				if selfDist <= elephantDist {
					nextArrival := selfDist + timeSpent
					my, myOpen := elephant(key, key2, selfDist+timeSpent-nextArrival, elephantDist2+timeSpent-nextArrival, minutesRemaining-nextArrival, open)
					if my > subPressure {
						subPressure = my
						newOpen = myOpen
					}
				} else {
					nextArrival := elephantDist + timeSpent
					my, myOpen := elephant(key2, key, selfDist2+timeSpent-nextArrival, elephantDist+timeSpent-nextArrival, minutesRemaining-nextArrival, open)
					if my > subPressure {
						subPressure = my
						newOpen = myOpen
					}
				}

			}
		}

		return subPressure + openPressureE + openPressure, newOpen
	}

	fmt.Println("starting elephant search")
	fcEStart := time.Now()
	maxPressure, maxOpen := elephant("AA", "AA", 0, 0, 26, "")
	fcEEnd := time.Now()
	fmt.Println(fcEEnd.Sub(fcEStart), maxPressure, strings.Trim(maxOpen, ","))

	if true {
		os.Exit(0)
	}

	var fcdfs func(current string, minutesRemaining int, open string) (int, string)
	fcdfs = func(current string, minutesRemaining int, open string) (int, string) {
		if minutesRemaining <= 0 {
			return 0, open
		}

		timeSpent := 0
		openPressure := 0
		if current != "AA" {
			timeSpent = 1
			openPressure += nodes[current].flow * (minutesRemaining - 1)
			// handle case where opening the current valve is best case
			open += current
		}

		newOpen := open
		subPressure := 0
		for n, d := range nodes[current].distances {
			if d > minutesRemaining-timeSpent || strings.Contains(open, n) {
				continue
			}
			p, o := fcdfs(n, minutesRemaining-d-timeSpent, open)
			if p > subPressure {
				subPressure = p
				newOpen = o
			}
		}

		return subPressure + openPressure, newOpen
	}

	fmt.Println("starting search")
	fcStart := time.Now()
	maxPressure, maxOpen = fcdfs("AA", 30, "")
	fcEnd := time.Now()
	fmt.Println(fcEnd.Sub(fcStart), maxPressure, maxOpen)

}
