package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer func(fp *os.File) {
		_ = fp.Close()
	}(fp)

	shapeScore := map[string]int{"X": 1, "Y": 2, "Z": 3}
	beats := map[string]string{"A": "Y", "B": "Z", "C": "X", "X": "B", "Y": "C", "Z": "A"}
	lose := map[string]string{"A": "Z", "B": "X", "C": "Y"}
	draw := map[string]string{"A": "X", "B": "Y", "C": "Z", "X": "A", "Y": "B", "Z": "C"}

	scorePartI := 0
	scorePartII := 0
	var opponent, me string
	//var line string
	for {
		_, err = fmt.Fscanln(fp, &opponent, &me)
		if err != nil {
			break
		}

		scorePartI += shapeScore[me]
		switch {
		case draw[opponent] == me:
			// draw
			scorePartI += 3
		case beats[opponent] == me:
			scorePartI += 6
		}

		var choice string
		switch me {
		case "X":
			// lose
			choice = lose[opponent]
		case "Y":
			//draw
			choice = draw[opponent]
			scorePartII += 3
		case "Z":
			//win
			choice = beats[opponent]
			scorePartII += 6
		}
		scorePartII += shapeScore[choice]
	}
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(scorePartI)
	fmt.Println(scorePartII)
}
