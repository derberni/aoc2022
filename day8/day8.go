package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

func max2(x, y int) int {
	if x >= y {
		return x
	}
	return y
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	var height [][]int

	for {
		var line string
		_, err = fmt.Fscanln(fp, &line)
		if err != nil {
			break
		}
		lineHeight := make([]int, 0, len(line))
		for _, r := range line {
			i, err := strconv.Atoi(string(r))
			if err != nil {
				log.Fatal(err)
			}
			lineHeight = append(lineHeight, i)
		}
		height = append(height, lineHeight)
	}

	yWidth := len(height)
	xWidth := len(height[0])

	highestLeft := make([][]int, yWidth)
	highestRight := make([][]int, yWidth)
	highestTop := make([][]int, yWidth)
	highestBottom := make([][]int, yWidth)
	for y := 0; y < yWidth; y++ {
		highestLeft[y] = make([]int, xWidth)
		highestRight[y] = make([]int, xWidth)
		highestTop[y] = make([]int, xWidth)
		highestBottom[y] = make([]int, xWidth)
	}

	for y := 0; y < yWidth; y++ {
		highestLeft[y][0] = height[y][0]
		highestLeft[y][1] = max2(height[y][0], height[y][1])
		for x := 2; x < xWidth; x++ {
			if height[y][x] > highestLeft[y][x-1] {
				highestLeft[y][x] = height[y][x]
			} else {
				highestLeft[y][x] = highestLeft[y][x-1]
			}
		}
	}

	for y := 0; y < yWidth; y++ {
		highestRight[y][xWidth-1] = height[y][xWidth-1]
		highestRight[y][xWidth-2] = max2(height[y][xWidth-1], height[y][xWidth-2])
		for x := xWidth - 3; x >= 0; x-- {
			if height[y][x] > highestRight[y][x+1] {
				highestRight[y][x] = height[y][x]
			} else {
				highestRight[y][x] = highestRight[y][x+1]
			}
		}
	}

	for x := 0; x < xWidth; x++ {
		highestTop[0][x] = height[0][x]
		highestTop[1][x] = max2(height[0][x], height[1][x])
		for y := 2; y < yWidth; y++ {
			if height[y][x] >= highestTop[y-1][x] {
				highestTop[y][x] = height[y][x]
			} else {
				highestTop[y][x] = highestTop[y-1][x]
			}
		}
	}

	for x := 0; x < xWidth; x++ {
		highestBottom[yWidth-1][x] = height[yWidth-1][x]
		highestBottom[yWidth-2][x] = max2(height[yWidth-1][x], height[yWidth-2][x])
		for y := yWidth - 3; y >= 0; y-- {
			if height[y][x] >= highestBottom[y+1][x] {
				highestBottom[y][x] = height[y][x]
			} else {
				highestBottom[y][x] = highestBottom[y+1][x]
			}
		}
	}

	fmt.Println(height)
	//fmt.Println(highestLeft)
	//fmt.Println(highestRight)
	//fmt.Println(highestTop)
	//fmt.Println(highestBottom)

	numVisible := 2*yWidth + 2*(xWidth-2)
	for y := 1; y < yWidth-1; y++ {
		for x := 1; x < xWidth-1; x++ {
			//fmt.Println(height[y][x], highestLeft[y][x], highestRight[y][x], highestTop[y][x], highestBottom[y][x], height[y][x] > highestLeft[y][x] || height[y][x] > highestRight[y][x] || height[y][x] > highestTop[y][x] || height[y][x] > highestBottom[y][x])
			if height[y][x] > highestLeft[y][x-1] || height[y][x] > highestRight[y][x+1] || height[y][x] > highestTop[y-1][x] || height[y][x] > highestBottom[y+1][x] {
				//visible[y][x] = 1
				numVisible++
			}
		}
	}
	fmt.Println(numVisible)

	start := time.Now()
	maxScore := 0
	score := make([][]int, yWidth)
	for y := 0; y < yWidth; y++ {
		score[y] = make([]int, xWidth)
		for x := 0; x < xWidth; x++ {
			leftScore := 1
			for l := x - 1; l > 0; l-- {
				if height[y][l] < height[y][x] {
					leftScore++
				} else {
					break
				}
			}
			rightScore := 1
			for r := x + 1; r < xWidth-1; r++ {
				if height[y][r] < height[y][x] {
					rightScore++
				} else {
					break
				}
			}
			topScore := 1
			for t := y - 1; t > 0; t-- {
				if height[t][x] < height[y][x] {
					topScore++
				} else {
					break
				}
			}
			bottomScore := 1
			for b := y + 1; b < yWidth-1; b++ {
				if height[b][x] < height[y][x] {
					bottomScore++
				} else {
					break
				}
			}
			score[y][x] = leftScore * rightScore * topScore * bottomScore
			if score[y][x] > maxScore {
				maxScore = score[y][x]
			}
		}
	}
	end := time.Now()
	fmt.Println(maxScore)
	fmt.Println(end.Sub(start))
}
