package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestRLE_add(t *testing.T) {
	tc := []struct {
		r    RLE
		i    Interval
		want RLE
	}{
		{
			r:    nil,
			i:    Interval{0, 10},
			want: RLE{Interval{0, 10}},
		},
		{
			r:    RLE{Interval{0, 10}},
			i:    Interval{0, 10},
			want: RLE{Interval{0, 10}},
		},
		{
			r:    RLE{Interval{6, 10}},
			i:    Interval{0, 5},
			want: RLE{Interval{0, 10}},
		},
	}

	for idx, tt := range tc {
		t.Run(fmt.Sprint(idx), func(t *testing.T) {
			got := tt.r.add(tt.i)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("got  %v\n want %v", got, tt.want)
			}
		})
	}
}
