package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"sort"
)

func min2(x, y int) int {
	if x <= y {
		return x
	}
	return y
}

func max2(x, y int) int {
	if x >= y {
		return x
	}
	return y
}

type position struct {
	x int
	y int
}

func (p position) mDistance(o position) int {
	return int(math.Abs(float64(p.x-o.x)) + math.Abs(float64(p.y-o.y)))
}

type Interval struct {
	start int
	end   int
}

func intersectsCore(i, o Interval) bool {
	return i.start >= o.start && i.start <= o.end || i.end >= o.start && i.end <= o.end
}

func (i Interval) intersects(o Interval) bool {
	return intersectsCore(i, o) || intersectsCore(o, i)
}

func (i Interval) clip(start, end int) Interval {
	return Interval{
		start: max2(i.start, start),
		end:   min2(i.end, end),
	}
}

type RLE []Interval

func (r RLE) Len() int {
	return len(r)
}

func (r RLE) Less(i, j int) bool {
	return r[i].end < r[j].start
}

func (r RLE) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

func (r RLE) add(i Interval) RLE {
	iLarger := Interval{i.start - 1, i.end + 1}
	intersectStart := -1
	intersectEnd := -1
Outer:
	for idx := 0; idx < len(r); idx++ {
		if r[idx].intersects(iLarger) {
			intersectStart = idx
			intersectEnd = idx
			for ; idx < len(r); idx++ {
				if r[idx].intersects(iLarger) {
					intersectEnd = idx
				} else {
					break Outer
				}
			}
		}
	}
	if intersectStart >= 0 {
		newInterval := Interval{
			start: min2(r[intersectStart].start, i.start),
			end:   max2(r[intersectEnd].end, i.end),
		}
		newRLE := append(r[:intersectStart], newInterval)
		if intersectEnd+1 < len(r) {
			return append(newRLE, r[intersectEnd+1:]...)
		} else {
			return newRLE
		}
	}
	newRLE := append(r, i)
	sort.Sort(newRLE)
	return newRLE
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)

	scannedArea := make(map[int]RLE)
	beacons := make(map[int][]int)

	for scanner.Scan() {
		line := scanner.Text()
		var sensor, beacon position
		_, err = fmt.Sscanf(line, "Sensor at x=%d, y=%d: closest beacon is at x=%d, y=%d", &sensor.x, &sensor.y, &beacon.x, &beacon.y)
		if err != nil {
			log.Fatal(err)
		}
		beacons[beacon.y] = append(beacons[beacon.y], beacon.x)
		dist := sensor.mDistance(beacon)
		scannedArea[sensor.y] = scannedArea[sensor.y].add(Interval{start: sensor.x - dist, end: sensor.x + dist})
		for i := 1; i < dist; i++ {
			currentInterval := Interval{start: sensor.x - dist + i, end: sensor.x + dist - i}
			scannedArea[sensor.y+i] = scannedArea[sensor.y+i].add(currentInterval)
			scannedArea[sensor.y-i] = scannedArea[sensor.y-i].add(currentInterval)
		}
	}

	//y := 10
	y := 2000000
	num := 0
	for _, i := range scannedArea[y] {
		num += i.end - i.start
	}
	fmt.Println(num)

	//maxSearch := 20
	maxSearch := 4000000
	testInterval := Interval{0, maxSearch}
Outer:
	for y := 0; y <= maxSearch; y++ {
		for _, i := range scannedArea[y] {
			if i.intersects(testInterval) {
				t := i.clip(0, maxSearch)
				if t.start != testInterval.start {
					fmt.Println(t.start-1, y, (t.start-1)*4000000+y)
					break Outer
				}
				if t.end != testInterval.end {
					fmt.Println(t.end+1, y, (t.end+1)*4000000+y)
					break Outer
				}
			}
		}
	}

}
