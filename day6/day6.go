package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	var line string
	_, err = fmt.Fscanln(fp, &line)
	if err != nil {
		log.Fatal(err)
	}

Outer:
	for i := 4; i < len(line); i++ {
		segment := line[i-4 : i]
		for _, r := range segment {
			if strings.Count(segment, string(r)) > 1 {
				continue Outer
			}
		}
		fmt.Println(segment)
		fmt.Println(i)
		break
	}

O2:
	for i := 14; i < len(line); i++ {
		segment := line[i-14 : i]
		for _, r := range segment {
			if strings.Count(segment, string(r)) > 1 {
				continue O2
			}
		}
		fmt.Println(segment)
		fmt.Println(i)
		break
	}
}
