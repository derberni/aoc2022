package main

import (
	"fmt"
	"log"
	"math"
	"os"
)

func mdist(i, j int, width int) int {
	xI := i % width
	yI := i / width
	xJ := j % width
	yJ := j / width

	return int(math.Abs(float64(xJ-xI)) + math.Abs(float64(yJ-yI)))
}

func canGo(now, next rune) bool {
	if next == 'S' {
		next = 'a'
	}
	if now == 'E' {
		now = 'z'
	}
	return now-next <= 1
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	var heights []rune
	var lineLength int
	var position, target int
	lineNum := 0
	for {
		var line string
		_, err = fmt.Fscan(fp, &line)
		if err != nil {
			break
		}
		lineLength = len(line)
		for i, r := range line {
			heights = append(heights, r)
			if r == 'E' {
				position = i + lineNum*lineLength
			}
			if r == 'S' {
				target = i + lineNum*lineLength
			}
		}
		lineNum++
	}

	steps := make(map[int]int)
	q := make(map[int]bool)
	for i := range heights {
		steps[i] = math.MaxInt
		q[i] = true
	}
	steps[position] = 0

	for len(q) > 0 {
		i := -1
		minDist := math.MaxInt
		for p := range q {
			if d := steps[p]; d <= minDist {
				minDist = d
				i = p
			}
		}
		delete(q, i)
		if minDist == math.MaxInt {
			continue
		}

		for next := range q {
			if mdist(i, next, lineLength) <= 1 && canGo(heights[i], heights[next]) {
				if a := minDist + 1; a < steps[next] {
					steps[next] = a
				}
			}
		}
	}

	fmt.Println("Part I:", steps[target])

	minA := math.MaxInt
	for i, s := range steps {
		if heights[i] == 'a' && s <= minA {
			minA = s
		}
	}

	fmt.Println("Part II:", minA)
}
