package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)
	scanner.Scan()
	directions := scanner.Text()

	arenaWidth := 7

	rockWidth := []int{4, 3, 3, 1, 2}
	rockHeight := []int{1, 3, 3, 4, 2}
	rockMask := [][]bool{
		{true, true, true, true},
		{false, true, false, true, true, true, false, true, false},
		{true, true, true, false, false, true, false, false, true},
		{true, true, true, true},
		{true, true, true, true},
	}

	currentHeight := 0
	rockType := 0
	rockY := currentHeight + 4
	rockX := 2
	step := 0
	totalStep := 0
	rockNum := 1

	hashDeposit := [2022 * 2]string{}
	heightByRockNum := [2022 * 2]int{}

	rockDeposit := [2022 * 2][7]bool{}
	for x := 0; x < arenaWidth; x++ {
		rockDeposit[0][x] = true
	}

	boolFormat := map[bool]string{true: "1", false: "0"}

	hashRock := func(rockX, rockY int) {
		hash := ""
		for y := 0; y < rockHeight[rockType]; y++ {
			for x := 0; x < arenaWidth; x++ {
				hash += boolFormat[rockDeposit[rockY+y][x]]
			}
			hash += fmt.Sprintf("%d%05d", rockType, step)
		}
		hashDeposit[rockNum] = hash
	}

	depositRock := func(rockX, rockY int) {
		for y := 0; y < rockHeight[rockType]; y++ {
			for x := 0; x < rockWidth[rockType]; x++ {
				rockDeposit[rockY+y][rockX+x] = rockMask[rockType][x+y*rockWidth[rockType]] || rockDeposit[rockY+y][rockX+x]
			}
		}
	}

	checkRepeat := func() (repeats bool, cycleLength int, cycleStart int) {
		if rockNum < 6 {
			return false, -1, -1
		}

		var cNum int
	Outer:
		for cNum = 5; cNum < rockNum; cNum++ {
			repeats = true
			for i := 0; i < 5; i++ {
				if hashDeposit[rockNum-i] != hashDeposit[cNum-i] {
					repeats = false
					continue Outer
				}
			}
			if repeats {
				break
			}
		}

		return repeats, rockNum - cNum, cNum
	}

	checkCollision := func(rockX, rockY int) bool {
		for y := 0; y < rockHeight[rockType]; y++ {
			for x := 0; x < rockWidth[rockType]; x++ {
				if rockDeposit[rockY+y][rockX+x] && rockMask[rockType][x+y*rockWidth[rockType]] {
					return true
				}
			}
		}
		return false
	}

	dumpDeposit := func(num int) {
		fp, err := os.Create(fmt.Sprintf("%5d.txt", num))
		if err != nil {
			log.Fatal(err)
		}
		defer fp.Close()
		for y := currentHeight + 5; y >= 0; y-- {
			for x := 0; x < 7; x++ {
				if x >= rockX && x < rockX+rockWidth[rockType] && y >= rockY && y < rockY+rockHeight[rockType] {
					if rockMask[rockType][x-rockX+(y-rockY)*rockWidth[rockType]] {
						_, _ = fp.WriteString("@")
					} else {
						if rockDeposit[y][x] {
							_, _ = fp.WriteString("#")
						} else {
							_, _ = fp.WriteString(".")
						}
					}
				} else {
					if rockDeposit[y][x] {
						_, _ = fp.WriteString("#")
					} else {
						_, _ = fp.WriteString(".")
					}
				}
			}
			_, _ = fp.WriteString("\n")
		}

	}

	dumpDeposit(step)

	var repeats bool
	var cycleLength, cycleStart int
	for {
		//dumpDeposit(totalStep)
		//fmt.Println(directions)
		//fmt.Print(strings.Repeat(" ", step))
		//fmt.Println("|")

		// move left/right
		rockNewX := rockX
		if directions[step] == '>' {
			if rockX+rockWidth[rockType] < arenaWidth {
				rockNewX += 1
			}
			//fmt.Println(totalStep, step, "rock", rockNum, "pushed right", rockNewX, rockY)
		} else {
			rockNewX = util.Max2(0, rockX-1)
			//fmt.Println(totalStep, step, "rock", rockNum, "pushed left", rockNewX, rockY)
		}
		// if we are below the current highest piece, check for collisions
		if !checkCollision(rockNewX, rockY) {
			rockX = rockNewX
		}

		// move down
		rockNewY := rockY - 1
		if checkCollision(rockX, rockNewY) {
			//fmt.Println(totalStep, step, "depositing rock at", rockX, rockY)
			depositRock(rockX, rockY)
			hashRock(rockX, rockY)

			currentHeight = util.Max2(rockY+rockHeight[rockType]-1, currentHeight)
			heightByRockNum[rockNum] = currentHeight
			repeats, cycleLength, cycleStart = checkRepeat()
			if repeats {
				break
			}
			//dumpDeposit(rockNum)
			//fmt.Println(totalStep, step, "new height", currentHeight)
			rockY = currentHeight + 4
			rockX = 2
			rockType = (rockType + 1) % 5
			rockNum++
		} else {
			rockY = rockNewY

		}

		if rockNum >= 2023 {
			break
		}

		step = (step + 1) % len(directions)
		totalStep++
	}

	heightCycleStart := heightByRockNum[cycleStart]
	heightCycleEnd := heightByRockNum[cycleStart+cycleLength]
	heightPerCycle := heightCycleEnd - heightCycleStart

	breakAt := 1000000000000
	fullCycles := (breakAt - cycleStart) / cycleLength
	remainingStones := breakAt - (fullCycles*cycleLength + cycleStart)
	fmt.Println(heightCycleStart + heightPerCycle*fullCycles + heightByRockNum[cycleStart+remainingStones] - heightByRockNum[cycleStart])

}
