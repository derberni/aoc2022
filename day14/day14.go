package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strings"
)

type position struct {
	x int
	y int
}

func (p position) equal(o position) bool {
	return p.x == o.x && p.y == o.y
}

func (p position) down() position {
	return position{x: p.x, y: p.y + 1}
}

func (p position) downLeft() position {
	return position{x: p.x - 1, y: p.y + 1}
}

func (p position) downRight() position {
	return position{x: p.x + 1, y: p.y + 1}
}

func (p position) connect(o position) []position {
	var con []position
	if diff := p.x - o.x; diff > 0 {
		for i := 0; i <= diff; i++ {
			con = append(con, position{x: o.x + i, y: o.y})
		}
	} else if diff := o.x - p.x; diff > 0 {
		for i := 0; i <= diff; i++ {
			con = append(con, position{x: p.x + i, y: p.y})
		}
	} else if diff := p.y - o.y; diff > 0 {
		for i := 0; i <= diff; i++ {
			con = append(con, position{x: o.x, y: o.y + i})
		}
	} else if diff := o.y - p.y; diff > 0 {
		for i := 0; i <= diff; i++ {
			con = append(con, position{x: p.x, y: p.y + i})
		}
	}
	return con
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)

	rock := make(map[position]bool)
	sand := make(map[position]bool)
	sand2 := make(map[position]bool)

	xMin := math.MaxInt
	yMin := math.MaxInt
	xMax := 0
	yMax := 0

	for scanner.Scan() {
		line := scanner.Text()

		coords := strings.Split(line, " -> ")
		positions := make([]position, 0, len(coords))
		for _, coord := range coords {
			p := position{}
			_, err = fmt.Sscanf(coord, "%d,%d", &p.x, &p.y)
			if err != nil {
				log.Fatal("invalid coordinate", err)
			}
			positions = append(positions, p)
		}
		for i := 0; i < len(positions)-1; i++ {
			for _, p := range positions[i].connect(positions[i+1]) {
				rock[p] = true
				if p.x < xMin {
					xMin = p.x
				}
				if p.x > xMax {
					xMax = p.x
				}
				if p.y < yMin {
					yMin = p.y
				}
				if p.y > yMax {
					yMax = p.y
				}
			}
		}
	}

	// Part I
	sandStart := position{x: 500, y: 0}
	currentPos := sandStart

Outer:
	for {
		for {
			nextPos := currentPos
			if !rock[currentPos.down()] && !sand[currentPos.down()] {
				nextPos = currentPos.down()
			} else if !rock[currentPos.downLeft()] && !sand[currentPos.downLeft()] {
				nextPos = currentPos.downLeft()
			} else if !rock[currentPos.downRight()] && !sand[currentPos.downRight()] {
				nextPos = currentPos.downRight()
			}
			if nextPos.equal(currentPos) {
				sand[currentPos] = true
				break
			}
			if nextPos.x <= xMin || nextPos.x >= xMax || nextPos.y > yMax {
				break Outer
			}
			currentPos = nextPos
		}
		currentPos = sandStart
	}

	fmt.Println("Part I:", len(sand))

	//Part II
	currentPos = sandStart

	bottomY := yMax + 2
	for x := xMin - 500; x < xMax+500; x++ {
		rock[position{x: x, y: bottomY}] = true
	}

Outer2:
	for {
		for {
			nextPos := currentPos
			if !rock[currentPos.down()] && !sand2[currentPos.down()] {
				nextPos = currentPos.down()
			} else if !rock[currentPos.downLeft()] && !sand2[currentPos.downLeft()] {
				nextPos = currentPos.downLeft()
			} else if !rock[currentPos.downRight()] && !sand2[currentPos.downRight()] {
				nextPos = currentPos.downRight()
			}
			if nextPos.equal(currentPos) {
				sand2[currentPos] = true
				if nextPos.equal(sandStart) {
					break Outer2
				}
				break
			}

			currentPos = nextPos
		}
		currentPos = sandStart
	}

	fmt.Println("Part II:", len(sand2))
}
