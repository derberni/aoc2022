package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
	"sort"
)

type ListItemType int

const (
	ItemInt ListItemType = iota
	ItemList
	ItemEmpty
)

type ListItem struct {
	i int
	l List
	t ListItemType
}

func (i ListItem) String() string {
	switch i.t {
	case ItemList:
		return i.l.String()
	case ItemInt:
		return fmt.Sprint(i.i)
	default:
		return ""
	}

}

func (i ListItem) compare(j ListItem) int {
	if i.t == ItemInt && j.t == ItemInt {
		if i.i < j.i {
			return -1
		} else if i.i > j.i {
			return 1
		} else {
			return 0
		}
	} else if i.t == ItemList && j.t == ItemList {
		return i.l.compare(j.l)
	} else {
		if i.t == ItemList && j.t == ItemInt {
			return i.l.compare(List{j})
		} else if i.t == ItemInt && j.t == ItemList {
			return List{i}.compare(j.l)
		} else {
			log.Fatal("should not reach this point")
			return 0
		}
	}
}

type List []ListItem

func (l List) String() string {
	var buf bytes.Buffer
	buf.WriteRune('[')
	for i := range l {
		buf.WriteString(l[i].String())
		if i != len(l)-1 {
			buf.WriteString(", ")
		}
	}
	buf.WriteRune(']')
	return buf.String()
}

func (l List) compare(o List) int {
	for i := range l {
		if i >= len(o) {
			return 1
		}
		less := l[i].compare(o[i])
		if less != 0 {
			return less
		}
	}
	if len(l) == len(o) {
		return 0
	}
	return -1
}

type ListSlice []List

func (l ListSlice) Len() int {
	return len(l)
}

func (l ListSlice) Less(i, j int) bool {
	return l[i].compare(l[j]) < 0
}

func (l ListSlice) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	input, err := io.ReadAll(fp)
	if err != nil {
		log.Fatal(err)
	}

	l := NewLexer(string(input))
	p := NewParser(l)

	message := p.ParseMessage()

	var correctOrder []int
	numCorrect := 0
	sum := 0
	for i := 0; i < len(message); i = i + 2 {
		if message[i].compare(message[i+1]) < 0 {
			index := (i / 2) + 1
			correctOrder = append(correctOrder, index)
			numCorrect++
			sum += index
		}
	}

	fmt.Println(correctOrder)
	fmt.Println(sum)

	m2 := List{ListItem{l: List{ListItem{i: 2, t: ItemInt}}, t: ItemList}}
	m6 := List{ListItem{l: List{ListItem{i: 6, t: ItemInt}}, t: ItemList}}
	message = append(message, m2, m6)

	sort.Sort(ListSlice(message))

	var i2, i6 int
	for i := 0; i < len(message); i++ {
		if reflect.DeepEqual(message[i], m2) {
			i2 = i + 1
		}
		if reflect.DeepEqual(message[i], m6) {
			i6 = i + 1
		}
	}
	fmt.Println(i2, i6, i2*i6)
}
