package main

import (
	"log"
	"strconv"
)

func NewParser(l *Lexer) *Parser {
	p := &Parser{l: l, signalStrength: []int{1}, cycle: 1}

	p.nextToken()
	p.nextToken()

	return p
}

type Parser struct {
	l *Lexer

	curToken  Token
	peekToken Token

	signalStrength []int
	cycle          int
}

func (p *Parser) nextToken() {
	p.curToken = p.peekToken
	p.peekToken = p.l.NextToken()
	if p.peekTokenIs(ILLEGAL) {
		log.Fatal("illegal token encountered", p.peekToken.Literal)
	}
}

func (p *Parser) ParseMessage() []List {
	var res []List

	if !p.curTokenIs(LBRACK) {
		log.Print("expected", LBRACK, "as first token")
		return nil
	}

	for !p.peekTokenIs(EOF) {
		res = append(res, p.parseList())
		p.nextToken()
	}

	return res
}

func (p *Parser) curTokenIs(tokenType TokenType) bool {
	return p.curToken.Type == tokenType
}

func (p *Parser) peekTokenIs(tokenType TokenType) bool {
	return p.peekToken.Type == tokenType
}

func (p *Parser) parseList() List {

	var res List

	//p.nextToken()
	//res = append(res, p.parseListItem())

	for !p.peekTokenIs(RBRACK) {
		p.nextToken()
		res = append(res, p.parseListItem())
	}

	p.nextToken()
	return res
}

func (p *Parser) parseListItem() ListItem {
	switch p.curToken.Type {
	case INT:
		i, err := strconv.Atoi(p.curToken.Literal)
		if err != nil {
			log.Fatal(err)
		}
		return ListItem{i: i, t: ItemInt}
	case LBRACK:
		return ListItem{l: p.parseList(), t: ItemList}
	default:
		return ListItem{t: ItemEmpty}
	}
}
