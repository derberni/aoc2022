package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"testing"
)

func TestMain_compare(t *testing.T) {
	fp, err := os.Open("testinput")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	input, err := io.ReadAll(fp)
	if err != nil {
		log.Fatal(err)
	}

	l := NewLexer(string(input))
	p := NewParser(l)

	message := p.ParseMessage()

	var tt []struct {
		l1      List
		l2      List
		correct bool
	}

	truth := []bool{true, true, false, true, false, true, false, false}

	for i := 0; i < len(message); i = i + 2 {
		tt = append(tt, struct {
			l1      List
			l2      List
			correct bool
		}{l1: message[i], l2: message[i+1], correct: truth[i/2]})
	}

	for i, tc := range tt {
		t.Run(fmt.Sprint(i), func(t *testing.T) {
			if got := tc.l1.compare(tc.l2); (got < 0) != tc.correct {
				t.Log(tc.l1, tc.l2)
				t.Errorf("wanted %t, got %t (%d)", tc.correct, got < 0, got)
			}
		})
	}
}
