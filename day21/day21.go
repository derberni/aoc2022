package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type op struct {
	m1 string
	m2 string
	o  byte
}

func (o op) do(a, b int) int {
	switch o.o {
	case '+':
		return a + b
	case '-':
		return a - b
	case '*':
		return a * b
	default:
		return a / b
	}
}

func (o op) leftInv(res, a int) int {
	switch o.o {
	case '+':
		return res - a
	case '-':
		return res + a
	case '*':
		return res / a
	default:
		return res * a
	}
}

func (o op) rightInv(res, b int) int {
	switch o.o {
	case '+':
		return res - b
	case '-':
		return b - res
	case '*':
		return res / b
	default:
		return b / res
	}
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)

	valueMonkeys := map[string]int{}

	calcMonkeys := map[string]op{}
	allCalcMonkeys := map[string]op{}

	humnMonkeys := map[string]op{}
	start := time.Now()

	for scanner.Scan() {
		line := scanner.Text()
		monkey := line[0:4]
		number, err := strconv.Atoi(line[6:])
		if err == nil {
			valueMonkeys[monkey] = number
			continue
		}
		rest := strings.Split(line[6:], " ")
		o := op{
			rest[0],
			rest[2],
			rest[1][0],
		}
		allCalcMonkeys[monkey] = o
		_, ok1 := humnMonkeys[o.m1]
		_, ok2 := humnMonkeys[o.m2]
		if ok1 || ok2 || o.m1 == "humn" || o.m2 == "humn" {
			humnMonkeys[monkey] = o
		}

		m1, ok1 := valueMonkeys[o.m1]
		m2, ok2 := valueMonkeys[o.m2]
		if ok1 && ok2 {
			valueMonkeys[monkey] = o.do(m1, m2)
		} else {
			calcMonkeys[monkey] = o
		}
	}

	for {
		if len(calcMonkeys) == 0 {
			break
		}
		for m, o := range calcMonkeys {
			m1, ok1 := valueMonkeys[o.m1]
			m2, ok2 := valueMonkeys[o.m2]
			if ok1 && ok2 {
				valueMonkeys[m] = o.do(m1, m2)
				delete(calcMonkeys, m)
			}
		}
	}

	for {
		if _, rootOk := humnMonkeys["root"]; rootOk {
			break
		}
		for m, o := range allCalcMonkeys {
			_, ok1 := humnMonkeys[o.m1]
			_, ok2 := humnMonkeys[o.m2]
			if ok1 || ok2 || o.m1 == "humn" || o.m2 == "humn" {
				humnMonkeys[m] = o
			}
		}
	}

	rootMonkey := allCalcMonkeys["root"]
	delete(allCalcMonkeys, "root")
	var humnRoot op
	var cmpValue int
	if m1, ok := humnMonkeys[rootMonkey.m1]; ok {
		humnRoot = m1
		cmpValue = valueMonkeys[rootMonkey.m2]
	} else {
		humnRoot = allCalcMonkeys[rootMonkey.m2]
		cmpValue = valueMonkeys[rootMonkey.m1]
	}

	currentMonkey := humnRoot
	currentVal := cmpValue

	for {
		if currentMonkey.m1 == "humn" {
			currentVal = currentMonkey.leftInv(currentVal, valueMonkeys[currentMonkey.m2])
			break
		} else if currentMonkey.m2 == "humn" {
			currentVal = currentMonkey.rightInv(currentVal, valueMonkeys[currentMonkey.m1])
			break
		}
		if m1, ok := humnMonkeys[currentMonkey.m1]; ok {
			currentVal = currentMonkey.leftInv(currentVal, valueMonkeys[currentMonkey.m2])
			currentMonkey = m1
		} else if m2, ok := humnMonkeys[currentMonkey.m2]; ok {
			currentVal = currentMonkey.rightInv(currentVal, valueMonkeys[currentMonkey.m1])
			currentMonkey = m2
		}
	}

	end := time.Now()
	fmt.Println("Part I:", end.Sub(start), valueMonkeys["root"])
	fmt.Println("Part II:", end.Sub(start), currentVal)
}
