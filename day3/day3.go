package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func priority(r rune) int {
	switch {
	case r >= 'a' && r <= 'z':
		return int(r) - 96
	case r >= 'A' && r <= 'Z':
		return int(r) - 64 + 26
	default:
		return 0
	}
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer func(fp *os.File) {
		_ = fp.Close()
	}(fp)

	var lines []string

	var totalPriority int
	for {
		var line string
		_, err = fmt.Fscanln(fp, &line)
		if err != nil {
			break
		}
		lines = append(lines, line)
		firstCompartment := line[:len(line)/2]
		secondCompartment := line[len(line)/2:]

		for _, r := range firstCompartment {
			if strings.ContainsRune(secondCompartment, r) {
				totalPriority += priority(r)
				break
			}
		}
	}

	fmt.Println(totalPriority)

	var badgePriority int
	for i := 0; i < len(lines)-2; i = i + 3 {
		for _, r := range lines[i] {
			if strings.ContainsRune(lines[i+1], r) && strings.ContainsRune(lines[i+2], r) {
				badgePriority += priority(r)
				break
			}
		}
	}

	fmt.Println(badgePriority)
}
