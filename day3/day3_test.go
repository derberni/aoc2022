package main

import "testing"

func Test_priority(t *testing.T) {

	tests := []struct {
		r    rune
		want int
	}{
		{
			r:    'a',
			want: 1,
		},
		{
			r:    'z',
			want: 26,
		},
		{
			r:    'A',
			want: 27,
		},
		{
			r:    'Z',
			want: 52,
		},
	}
	for _, tt := range tests {
		t.Run(string(tt.r), func(t *testing.T) {
			if got := priority(tt.r); got != tt.want {
				t.Errorf("priority() = %v, want %v", got, tt.want)
			}
		})
	}
}
