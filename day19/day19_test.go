package main

import (
	"fmt"
	"math/rand"
	"testing"
)

func BenchmarkShift(b *testing.B) {

	resources := [4]uint8{1, 255, 3, 4}
	robots := [4]uint8{1, 2, 3, 255}
	timeRemaining := 32

	for i := 0; i < b.N; i++ {
		s := uint64(timeRemaining)<<56 | uint64(resources[0])<<0 | uint64(resources[1])<<6 | uint64(resources[2])<<12 | uint64(resources[3])<<18 | uint64(robots[0])<<24 | uint64(robots[1])<<30 | uint64(robots[2])<<36 | uint64(robots[3])<<42
		_ = s
	}
}

func BenchmarkShift_BoundsOptimized(b *testing.B) {
	resources := [4]uint8{1, 255, 3, 4}
	robots := [4]uint8{1, 2, 3, 255}
	timeRemaining := 32

	for i := 0; i < b.N; i++ {
		s := uint64(timeRemaining)<<56 | uint64(resources[3])<<0 | uint64(resources[2])<<6 | uint64(resources[1])<<12 | uint64(resources[0])<<18 | uint64(robots[3])<<24 | uint64(robots[2])<<30 | uint64(robots[1])<<36 | uint64(robots[0])<<42
		_ = s
	}
}

func BenchmarkMult(b *testing.B) {

	resources := [4]uint8{1, 255, 3, 4}
	robots := [4]uint8{1, 2, 3, 255}
	timeRemaining := 32

	for i := 0; i < b.N; i++ {
		s := int64(timeRemaining) +
			int64(32+1)*int64(robots[0]) +
			int64(32+1)*5*int64(robots[1]) +
			int64(32+1)*5*5*int64(robots[2]) +
			int64(32+1)*5*5*5*int64(robots[3]) +
			int64(32+1)*5*5*5*5*int64(resources[0]) +
			int64(32+1)*5*5*5*5*5*int64(resources[1]) +
			int64(32+1)*5*5*5*5*5*5*int64(resources[2]) +
			int64(32+1)*5*5*5*5*5*5*5*int64(resources[3])
		_ = s
	}
}

func BenchmarkMap_Mult(b *testing.B) {
	m := make(map[int64]struct{}, 1000)

	rand.Seed(3141)

	for i := 0; i < b.N; i++ {
		r := rand.Uint32()
		resources := [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)}
		robots := [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)}
		timeRemaining := 32
		s := int64(timeRemaining) +
			int64(32+1)*int64(robots[0]) +
			int64(32+1)*5*int64(robots[1]) +
			int64(32+1)*5*5*int64(robots[2]) +
			int64(32+1)*5*5*5*int64(robots[3]) +
			int64(32+1)*5*5*5*5*int64(resources[0]) +
			int64(32+1)*5*5*5*5*5*int64(resources[1]) +
			int64(32+1)*5*5*5*5*5*5*int64(resources[2]) +
			int64(32+1)*5*5*5*5*5*5*5*int64(resources[3])
		m[s] = struct{}{}
	}
}

func BenchmarkMap_Mult64(b *testing.B) {
	m := make(map[int64]struct{}, 1000)

	rand.Seed(3141)

	for i := 0; i < b.N; i++ {
		r := rand.Uint32()
		resources := [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)}
		robots := [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)}
		timeRemaining := 32
		s := int64(timeRemaining) +
			int64(64)*int64(robots[0]) +
			int64(64)*64*int64(robots[1]) +
			int64(64)*64*64*int64(robots[2]) +
			int64(64)*64*64*64*int64(robots[3]) +
			int64(64)*64*64*64*64*int64(resources[0]) +
			int64(64)*64*64*64*64*64*int64(resources[1]) +
			int64(64)*64*64*64*64*64*64*int64(resources[2]) +
			int64(64)*64*64*64*64*64*64*64*int64(resources[3])
		m[s] = struct{}{}
	}
}

func BenchmarkMap_Shift(b *testing.B) {
	m := make(map[int64]struct{}, 1000)

	rand.Seed(3141)

	for i := 0; i < b.N; i++ {
		r := rand.Uint32()
		resources := [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)}
		robots := [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)}
		timeRemaining := 32

		s := int64(timeRemaining)<<56 | int64(resources[0])<<0 | int64(resources[1])<<6 | int64(resources[2])<<12 | int64(resources[3])<<18 | int64(robots[0])<<24 | int64(robots[1])<<30 | int64(robots[2])<<36 | int64(robots[3])<<42
		m[s] = struct{}{}
	}
}

func BenchmarkMap_String(b *testing.B) {
	m := make(map[string]struct{}, 1000)

	rand.Seed(3141)

	for i := 0; i < b.N; i++ {
		r := rand.Uint32()
		resources := [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)}
		robots := [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)}
		timeRemaining := 32

		s := fmt.Sprintf("%06d%06d%06d%06d%06d%06d%06d%06d%06d", timeRemaining, resources[0], resources[1], resources[2], resources[3], robots[0], robots[1], robots[2], robots[3])
		m[s] = struct{}{}
	}
}

type sstruct struct {
	a [4]uint8
	b [4]uint8
	t int
}

func BenchmarkMap_Struct(b *testing.B) {
	m := make(map[sstruct]struct{}, 1000)

	rand.Seed(3141)

	for i := 0; i < b.N; i++ {
		r := rand.Uint32()
		s := sstruct{
			a: [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)},
			b: [4]uint8{uint8(r), uint8(r >> 8), uint8(r >> 16), uint8(r >> 24)},
			t: 32,
		}

		m[s] = struct{}{}
	}
}
