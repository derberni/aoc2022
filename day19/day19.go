package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"time"
)

var memo map[int64]uint8

func dfs(resources [4]int, robots [4]int, costs [4][4]int, timeRemaining int) uint8 {
	if timeRemaining <= 0 {
		return uint8(resources[3])
	}
	s := int64(timeRemaining) +
		int64(32+1)*int64(robots[0]) +
		int64(32+1)*5*int64(robots[1]) +
		int64(32+1)*5*5*int64(robots[2]) +
		int64(32+1)*5*5*5*int64(robots[3]) +
		int64(32+1)*5*5*5*5*int64(resources[0]) +
		int64(32+1)*5*5*5*5*5*int64(resources[1]) +
		int64(32+1)*5*5*5*5*5*5*int64(resources[2]) +
		int64(32+1)*5*5*5*5*5*5*5*int64(resources[3])

	if m, ok := memo[s]; ok {
		//fmt.Println(timeRemaining, resources, robots, "from memo")
		return m
	}

	//fmt.Println(timeRemaining, resources, robots)
	maxGeodes := uint8(0)

	for r := 0; r < 4; r++ {
		if !(resources[0] >= costs[r][0] && resources[1] >= costs[r][1] && resources[2] >= costs[r][2]) {
			continue
		}
		// harvest
		newResources := [4]int{
			resources[0] + robots[0] - costs[r][0],
			resources[1] + robots[1] - costs[r][1],
			resources[2] + robots[2] - costs[r][2],
			resources[3] + robots[3],
		}

		newRobots := robots
		newRobots[r]++
		geodes := dfs(newResources, newRobots, costs, timeRemaining-1)
		if geodes > maxGeodes {
			maxGeodes = geodes
		}
	}
	newResources := [4]int{
		resources[0] + robots[0],
		resources[1] + robots[1],
		resources[2] + robots[2],
		resources[3] + robots[3],
	}
	geodes := dfs(newResources, robots, costs, timeRemaining-1)
	if geodes > maxGeodes {
		maxGeodes = geodes
	}

	memo[s] = maxGeodes

	return maxGeodes
}

func main() {
	fp, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)
	scanner.Split(bufio.ScanWords)

	numbers := []int{}
	for scanner.Scan() {
		if i, err := strconv.Atoi(scanner.Text()); err == nil {
			numbers = append(numbers, i)
		}
	}

	L := 32

	var qualityLevel int
	//workers := 1
	numJobs := util.Min2(3, len(numbers)/6)
	results := make([]int64, numJobs)

	start := time.Now()
	for bp := 0; bp < numJobs; bp++ {
		var costs [4][4]int
		costs[0][0] = numbers[bp*6]
		costs[1][0] = numbers[bp*6+1]
		costs[2][0] = numbers[bp*6+2]
		costs[2][1] = numbers[bp*6+3]
		costs[3][0] = numbers[bp*6+4]
		costs[3][2] = numbers[bp*6+5]

		memo = make(map[int64]uint8, math.MaxInt16)

		st := time.Now()
		geodes := dfs([4]int{0, 0, 0, 0}, [4]int{1, 0, 0, 0}, costs, L)
		et := time.Now()
		ql := int(geodes) * (bp + 1)
		fmt.Println(et.Sub(st), geodes)
		qualityLevel += ql
		results[bp] = int64(geodes)
	}

	end := time.Now()
	fmt.Println()
	fmt.Println(end.Sub(start), qualityLevel)
	prod := int64(1)
	for _, r := range results {
		prod *= r
	}
	fmt.Println(prod)
}
