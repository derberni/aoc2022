package util

import "golang.org/x/exp/constraints"

func Min2[T constraints.Ordered](x, y T) T {
	if x <= y {
		return x
	}
	return y
}

func Max2[T constraints.Ordered](x, y T) T {
	if x >= y {
		return x
	}
	return y
}
