package util

func MapAny[M ~map[K]V, K comparable, V any](m M, m2 M) bool {
	for o := range m2 {
		if _, ok := m[o]; ok {
			return true
		}
	}
	return false
}
