package util

type Vec2 struct {
	X int
	Y int
}

func (v Vec2) Add(o Vec2) Vec2 {
	return Vec2{Y: v.Y + o.Y, X: v.X + o.X}
}

func (v Vec2) Mod(o Vec2) Vec2 {
	return Vec2{Y: v.Y % o.Y, X: v.X % o.X}
}

func (v Vec2) Neighbors4() map[Vec2]bool {
	return map[Vec2]bool{
		{v.X + 1, v.Y}: true,
		{v.X - 1, v.Y}: true,
		{v.X, v.Y + 1}: true,
		{v.X, v.Y - 1}: true,
	}
}

func (v Vec2) Neighbors8() map[Vec2]bool {
	return map[Vec2]bool{
		{v.X + 1, v.Y}:     true,
		{v.X + 1, v.Y - 1}: true,
		{v.X + 1, v.Y + 1}: true,
		{v.X - 1, v.Y}:     true,
		{v.X - 1, v.Y - 1}: true,
		{v.X - 1, v.Y + 1}: true,
		{v.X, v.Y + 1}:     true,
		{v.X, v.Y - 1}:     true,
	}
}

func (v Vec2) NeighborsNorth() map[Vec2]bool {
	return map[Vec2]bool{
		{v.X + 1, v.Y - 1}: true,
		{v.X - 1, v.Y - 1}: true,
		{v.X, v.Y - 1}:     true,
	}
}

func (v Vec2) NeighborsSouth() map[Vec2]bool {
	return map[Vec2]bool{
		{v.X + 1, v.Y + 1}: true,
		{v.X - 1, v.Y + 1}: true,
		{v.X, v.Y + 1}:     true,
	}
}

func (v Vec2) NeighborsWest() map[Vec2]bool {
	return map[Vec2]bool{
		{v.X - 1, v.Y}:     true,
		{v.X - 1, v.Y - 1}: true,
		{v.X - 1, v.Y + 1}: true,
	}
}

func (v Vec2) NeighborsEast() map[Vec2]bool {
	return map[Vec2]bool{
		{v.X + 1, v.Y}:     true,
		{v.X + 1, v.Y - 1}: true,
		{v.X + 1, v.Y + 1}: true,
	}
}
