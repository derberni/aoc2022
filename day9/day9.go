package main

import (
	"fmt"
	"log"
	"os"
)

type position struct {
	x int
	y int
}

func (p position) move(direction string) position {
	switch direction {
	case "U":
		p.y++
	case "D":
		p.y--
	case "L":
		p.x--
	case "R":
		p.x++
	default:
		log.Fatal("invalid direction")
	}
	return p
}

func (p position) follow(o position) position {
	t := p
	switch {
	case o.x-t.x > 1:
		if o.y-t.y > 0 {
			t = t.move("U")
		} else if o.y-t.y < 0 {
			t = t.move("D")
		}
		t = t.move("R")
	case t.x-o.x > 1:
		if o.y-t.y > 0 {
			t = t.move("U")
		} else if o.y-t.y < 0 {
			t = t.move("D")
		}
		t = t.move("L")
	case o.y-t.y > 1:
		if o.x-t.x > 0 {
			t = t.move("R")
		} else if o.x-t.x < 0 {
			t = t.move("L")
		}
		t = t.move("U")
	case t.y-o.y > 1:
		if o.x-t.x > 0 {
			t = t.move("R")
		} else if o.x-t.x < 0 {
			t = t.move("L")
		}
		t = t.move("D")
	}
	return t
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	visited := map[position]struct{}{position{0, 0}: {}}
	head := position{}
	tail := position{}

	head2tail := make([]position, 10)
	visited2 := map[position]struct{}{position{0, 0}: {}}

	for {
		var direction string
		var steps int
		_, err = fmt.Fscanln(fp, &direction, &steps)
		if err != nil {
			break
		}

		for i := 0; i < steps; i++ {
			head = head.move(direction)
			tail = tail.follow(head)
			//fmt.Println(head, tail)
			visited[tail] = struct{}{}

			head2tail[9] = head2tail[9].move(direction)
			for i := 8; i >= 0; i-- {
				head2tail[i] = head2tail[i].follow(head2tail[i+1])
			}
			visited2[head2tail[0]] = struct{}{}
		}
	}

	numVisited := 0
	for range visited {
		numVisited++
	}
	fmt.Println(numVisited)

	numVisited2 := 0
	for range visited2 {
		numVisited2++
	}
	fmt.Println(numVisited2)
}
