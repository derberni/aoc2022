package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer func(fp *os.File) {
		_ = fp.Close()
	}(fp)

	var numContained int
	var numOverlapped int
	for {
		var start1, start2, end1, end2 int
		_, err = fmt.Fscanf(fp, "%d-%d,%d-%d", &start1, &end1, &start2, &end2)
		if err != nil {
			break
		}
		// range fully contained
		if start1 >= start2 && end1 <= end2 {
			numContained++
		} else if start2 >= start1 && end2 <= end1 {
			numContained++
		}
		// ranges overlap
		switch {
		case start1 >= start2 && start1 <= end2:
			numOverlapped++
		case end1 >= start2 && end1 <= end2:
			numOverlapped++
		case start2 >= start1 && start2 <= end1:
			numOverlapped++
		case end2 >= start1 && end2 <= end1:
			numOverlapped++
		}
	}
	fmt.Println(numContained)
	fmt.Println(numOverlapped)
}
