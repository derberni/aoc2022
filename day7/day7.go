package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {

	fp, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	input, err := io.ReadAll(fp)
	if err != nil {
		log.Fatal(err)
	}

	l := NewLexer(string(input))

	p := NewParser(l)
	rootDir := p.ParseScript()

	searchSize := 100000

	var res []int
	var calcSizes func(d *Directory)
	calcSizes = func(d *Directory) {
		for _, f := range d.Files {
			d.Size += f.Size
		}
		for _, c := range d.Children {
			calcSizes(c)
			d.Size += c.Size
		}
		if d.Size <= searchSize {
			res = append(res, d.Size)
		}
	}

	calcSizes(rootDir)

	sum := 0
	for _, i := range res {
		sum += i
	}

	fmt.Println("Part I:")
	fmt.Println(sum)

	// Part II
	totalSpace := 70000000
	requiredSpace := 30000000
	neededSpace := requiredSpace - (totalSpace - rootDir.Size)

	toFree := rootDir.Size
	var searchFn func(d *Directory)
	searchFn = func(d *Directory) {
		if d.Size >= neededSpace && d.Size <= toFree {
			toFree = d.Size
		}
		for _, c := range d.Children {
			searchFn(c)
		}
	}
	searchFn(rootDir)
	fmt.Println("Part II:")
	fmt.Println(toFree)

}
