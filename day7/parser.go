package main

import (
	"log"
	"strconv"
)

func NewParser(l *Lexer) *Parser {
	p := &Parser{l: l}

	p.nextToken()
	p.nextToken()

	return p
}

type Parser struct {
	l *Lexer

	curToken  Token
	peekToken Token

	rootDirectory *Directory
	curDirectory  *Directory
}

func (p *Parser) nextToken() {
	p.curToken = p.peekToken
	p.peekToken = p.l.NextToken()
	if p.peekTokenIs(ILLEGAL) {
		log.Fatal("illegal token encountered")
	}
}

func (p *Parser) ParseScript() *Directory {
	p.rootDirectory = &Directory{
		Name: "/",
	}
	p.curDirectory = p.rootDirectory

	for !p.peekTokenIs(EOF) {
		p.parseCommand()
	}

	return p.rootDirectory
}

func (p *Parser) curTokenIs(tokenType TokenType) bool {
	return p.curToken.Type == tokenType
}

func (p *Parser) peekTokenIs(tokenType TokenType) bool {
	return p.peekToken.Type == tokenType
}

func (p *Parser) parseCommand() {
	p.nextToken()
	switch p.curToken.Type {
	case CHDIR:
		p.parseCD()
	case LISTDIR:
		p.parseLS()
	}

}

func (p *Parser) parseLS() {
	p.nextToken()

	for !p.curTokenIs(CMD) && !p.curTokenIs(EOF) {
		if p.curTokenIs(IDENT) && p.curToken.Literal == "dir" {
			p.parseDirEntry()
		} else if p.curTokenIs(INT) {
			p.parseFileEntry()
		}
		p.nextToken()
		p.nextToken()
	}
}

func (p *Parser) parseDirEntry() {
	p.curDirectory.Children = append(p.curDirectory.Children, &Directory{
		Name:   p.peekToken.Literal,
		Parent: p.curDirectory,
	})
}

func (p *Parser) parseFileEntry() {
	size, err := strconv.Atoi(p.curToken.Literal)
	if err != nil {
		size = -1
	}
	p.curDirectory.Files = append(p.curDirectory.Files, File{
		Size: size,
		Name: p.peekToken.Literal,
	})
}

func (p *Parser) parseCD() {
	p.nextToken()
	switch p.curToken.Literal {
	case "/":
		p.curDirectory = p.rootDirectory
	case "..":
		p.curDirectory = p.curDirectory.Parent
	default:
		for _, child := range p.curDirectory.Children {
			if child.Name == p.curToken.Literal {
				p.curDirectory = child
			}
		}
	}
	p.nextToken()
}
