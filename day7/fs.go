package main

type Directory struct {
	Parent   *Directory
	Children []*Directory
	Files    []File

	Name string
	Size int
}

type File struct {
	Size int
	Name string
}
