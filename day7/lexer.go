package main

import (
	"unicode"
	"unicode/utf8"
)

func NewLexer(input string) *Lexer {
	l := &Lexer{input: input}

	l.readChar()

	return l
}

type Lexer struct {
	input string

	ch rune

	// position is the position of the current char
	position int
	// readPosition is the position of the next char to read
	readPosition int
}

func (l *Lexer) NextToken() Token {
	var tok Token

	l.skipWhitespace()

	switch l.ch {
	case '$':
		tok = Token{
			Type:    CMD,
			Literal: "$",
		}
	case 0:
		tok.Literal = ""
		tok.Type = EOF
	default:
		if isLetter(l.ch) {
			ident := l.readIdentifier()
			tok = Token{
				Type:    LookupIdent(ident),
				Literal: ident,
			}
			return tok
		} else if isDigit(l.ch) {
			tok.Literal = l.readInteger()
			tok.Type = INT
			return tok
		} else {
			tok = newToken(ILLEGAL, string(l.ch))
		}
	}

	l.readChar()
	return tok
}

func (l *Lexer) skipWhitespace() {
	for unicode.IsSpace(l.ch) {
		l.readChar()
	}
}

func (l *Lexer) readIdentifier() string {
	startPos := l.position

	for isLetter(l.ch) {
		l.readChar()
	}

	return l.input[startPos:l.position]
}

func (l *Lexer) readInteger() string {
	startPos := l.position

	for isDigit(l.ch) {
		l.readChar()
	}

	return l.input[startPos:l.position]
}

func (l *Lexer) readChar() {
	var width int
	if l.readPosition >= len(l.input) {
		l.ch = 0
	} else {
		l.ch, width = utf8.DecodeRuneInString(l.input[l.readPosition:])
	}
	l.position = l.readPosition
	l.readPosition += width
}

func (l *Lexer) peekChar() byte {
	if l.readPosition >= len(l.input) {
		return 0
	} else {
		return l.input[l.readPosition]
	}
}

func newToken(t TokenType, l string) Token {
	return Token{
		Type:    t,
		Literal: l,
	}
}

func isLetter(ch rune) bool {
	return unicode.IsLetter(ch) || unicode.IsSymbol(ch) || ch == '.' || ch == '/'
}

func isDigit(ch rune) bool {
	return unicode.IsDigit(ch)
}
